---
layout: obra
thumbnail: /assets/thumbnail/obra_6.png
artista: Rosa Schramm
title: Giro
material: Vídeo
dimensao: 9'28"
ano: 2019
bio: https://www.instagram.com/rosa.schramm/
cor : 00EBB7
---
<iframe src="https://www.youtube.com/embed/GfgRZdqBPgo" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<br>