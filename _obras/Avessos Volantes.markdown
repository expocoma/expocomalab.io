---
layout: obra
thumbnail: /assets/thumbnail/obra_23.png
artista: Thaís Oliveira
title: Avessos Volantes
material: Cola PVA e linha de costura
dimensao: Dimensões Variáveis
ano: 2021
bio: https://thaisoliveiraw.wordpress.com/
cor: 00EBB7
---
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
 
  	<!-- itens -->
  <div class="carousel-item active">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/thais/t-1.jpg/?auto=no&bg=777&fg=555&text=A1" alt="A1">
    </div>
  <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/thais/t-2.jpg/?auto=no&bg=666&fg=444&text=A2" alt="A2">
    </div>
   <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/thais/t-3.jpg/?auto=no&bg=666&fg=444&text=A3" alt="A3">
    </div>
      <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/thais/t-4.jpg/?auto=no&bg=666&fg=444&text=A4" alt="A4">
    </div>
      <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/thais/t-5.jpg/?auto=no&bg=666&fg=444&text=A5" alt="A5">
    </div>
      <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/thais/t-6.jpg/?auto=no&bg=666&fg=444&text=B1" alt="B1">
    </div>
      <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/thais/t-7.jpg/?auto=no&bg=666&fg=444&text=B2" alt="B2">
    </div>
      <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/thais/t-8.jpg/?auto=no&bg=666&fg=444&text=B3" alt="B3">
    </div>
      <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/thais/t-9.jpg/?auto=no&bg=666&fg=444&text=B4" alt="B4">
    </div>
      <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/thais/t-10.jpg/?auto=no&bg=666&fg=444&text=B5" alt="B5">
    </div>
      <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/thais/t-11.jpg/?auto=no&bg=666&fg=444&text=C1" alt="C1">
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/thais/t-12.jpg/?auto=no&bg=666&fg=444&text=C2" alt="C2">
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/thais/t-13.jpg/?auto=no&bg=666&fg=444&text=C3" alt="C4">
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/thais/t-14.jpg/?auto=no&bg=666&fg=444&text=C4" alt="C4">
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/thais/t-15.jpg/?auto=no&bg=666&fg=444&text=C5" alt="C5">
    </div>
    <!-- fim itens -->
  </div>
  
  <!-- controle --> 
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Anterior</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Próximo</span>
  </a>
</div>