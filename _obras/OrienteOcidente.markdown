---
layout: obra
thumbnail: /assets/thumbnail/obra_25.png
artista: Alina Duchrow
title: Oriente-se-Ocidente
material: Vídeo
dimensao: 2' min
ano: 2021
bio: www.alinaduchrow.com
cor: 00BDFF
---
<iframe src="https://www.youtube.com/embed/k6NBrD-zq88" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<br>