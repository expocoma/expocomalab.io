---
layout: obra
thumbnail: /assets/thumbnail/obra_26.png
artista: Taís Koshino
title: Jardins de fuga
material: Imagem Digital
dimensao: Imagens criadas dentro de jardim | 枯山水 | garden entre 08 de novembro e 03 de dezembro de 2021
ano: 2021
bio: https://taiskoshino.com/
cor: 00EBB7
---

<iframe style="width: 200%; height: 900px; transform: scale(0.5); transform-origin: 0 0; margin-bottom: -30em;border: none;" src="https://taiskoshino.com/jardins-de-fuga/"></iframe>

<a href="https://taiskoshino.com/jardins-de-fuga/" target="_blank">TELA INTEIRA</a>

<br>
<p>Jardins de fuga criados dentro da simulação <a href="/jardim-garden">jardim | 枯山水 | garden </a> durante a exposição <br>
“Linhas de Fuga”, parte da programação do IX CoMA.<br>
Atualizado toda terça e sexta de 08/11 a 03/12.</p>
<p>jardins de fuga (scape gardens) created inside <a href="/jardim-garden">jardim | 枯山水 | garden </a> at<br>
“Linhas de Fuga” exhibition at IX CoMA.<br>
It is updated every Tuesday and Friday between 11/08 and 12/03.</p>