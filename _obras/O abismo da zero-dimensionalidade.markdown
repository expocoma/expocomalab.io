---
layout: obra
thumbnail: /assets/thumbnail/obra_10.png
artista: Havane Melo
title: O abismo da zero-dimensionalidade
material: Vídeo
dimensao: 1'32''
ano: 2021
bio: https://www.havanemelo.com/
cor: 00BDFF
---
<iframe src="https://www.youtube.com/embed/z3o0K9_ftQA" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<br>
