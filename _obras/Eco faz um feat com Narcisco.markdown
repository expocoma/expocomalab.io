---
layout: obra
thumbnail: /assets/thumbnail/obra_24.png
artista: Xikão Xikão
title: Eco faz um feat com Narciso (Série)
material: Vídeo
dimensao: 5'15'', 3'51'', 4'10''
ano: 2021
bio: https://xikaoxikao.com/
cor: 969696
---
<iframe src="https://www.youtube.com/embed/DYK_QTiI-6o" width="468" height="832" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<br>
<iframe src="https://www.youtube.com/embed/-aDl8jSlpeY" width="468" height="832" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<br>
<iframe src="https://www.youtube.com/embed/6BwlN-OBwpg" width="468" height="832" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<br>