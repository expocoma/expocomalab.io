---
layout: obra
thumbnail: /assets/thumbnail/obra_7.png
artista: Lynn Carone
title: Samsara (Série em processo)
material: Vídeo
dimensao: 3'42''
ano: 2021
bio:  https://lynncarone.com/home/
cor: 00BDFF
---
<iframe src="https://www.youtube.com/embed/_CklGpq_wLs" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<br>
