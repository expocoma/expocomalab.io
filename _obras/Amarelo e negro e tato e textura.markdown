---
layout: obra
thumbnail: /assets/thumbnail/obra_9.png
artista: Letícia Miranda
title: Amarelo e negro e tato e textura (Políptico)
material: Papel, giz de cera e cola
dimensao: 5 x 5 cm cada
ano: 2021
bio: http://www.leticiamiranda.net/
cor: 00EBB7
---
<style>
#myImg {
  cursor: pointer;
  transition: 0.3s;
}

#myImg:hover {
  opacity: 0.7;
}

.modal {
  display: none;
  /* Hidden by default */
  position: fixed;
  /* Stay in place */
  z-index: 1;
  /* Sit on top */
  padding-top: 100px;
  /* Location of the box */
  left: 0;
  top: 0;
  width: 100%;
  /* Full width */
  height: 100%;
  /* Full height */
  overflow: auto;
  /* Enable scroll if needed */
  background-color: rgb(0, 0, 0);
  /* Fallback color */
  background-color: rgba(0, 0, 0, 0.9);
  /* Black w/ opacity */
}

.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

.modal-content,
#caption {
  animation-name: zoom;
  animation-duration: 0.6s;
}

@keyframes zoom {
  from {
    transform: scale(0)
  }
  to {
    transform: scale(1)
  }
}

.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

@media only screen and (max-width: 700px) {
  .modal-content {
    width: 100%;
  }
}
</style>


<img class="myImages" id="myImg" src="/obras-externas/leticia/l-1.jpg" alt="Foto 01" height="150">
<img class="myImages" id="myImg" src="/obras-externas/leticia/l-2.jpg" alt="Foto 02" height="150">
<img class="myImages" id="myImg" src="/obras-externas/leticia/l-3.jpg" alt="Foto 03" height="150">

<img class="myImages" id="myImg" src="/obras-externas/leticia/l-4.jpg" alt="Foto 04" height="150">
<img class="myImages" id="myImg" src="/obras-externas/leticia/l-5.jpg" alt="Foto 05" height="150">
<img class="myImages" id="myImg" src="/obras-externas/leticia/l-6.jpg" alt="Foto 06" height="150">

<img class="myImages" id="myImg" src="/obras-externas/leticia/l-7.jpg" alt="Foto 07" height="150">
<img class="myImages" id="myImg" src="/obras-externas/leticia/l-8.jpg" alt="Foto 08" height="150">
<img class="myImages" id="myImg" src="/obras-externas/leticia/l-9.jpg" alt="Foto 09" height="150">

<img class="myImages" id="myImg" src="/obras-externas/leticia/l-10.jpg" alt="Foto 10" height="150">
<img class="myImages" id="myImg" src="/obras-externas/leticia/l-11.jpg" alt="Foto 11" height="150">
<img class="myImages" id="myImg" src="/obras-externas/leticia/l-12.jpg" alt="Foto 12" height="150">


<a href="" target="_blank">


<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
</div>



<script>
// create references to the modal...
var modal = document.getElementById('myModal');
// to all images -- note I'm using a class!
var images = document.getElementsByClassName('myImages');
// the image in the modal
var modalImg = document.getElementById("img01");
// and the caption in the modal

// Go through all of the images with our custom class
for (var i = 0; i < images.length; i++) {
  var img = images[i];
  // and attach our click listener for this image.
  img.onclick = function(evt) {
    console.log(evt);
    modal.style.display = "block";
    modalImg.src = this.src;
  }
}

var span = document.getElementsByClassName("close")[0];

span.onclick = function() {
  modal.style.display = "none";
}
</script>
