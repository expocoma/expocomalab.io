---
layout: obra
thumbnail: /assets/thumbnail/obra_17.png
artista: leaColumbi
title: Pensar estudante fugado
material: performance colaborativa online
dimensao: 30'
ano: 2021
bio: https://leamucho.wixsite.com/portafolio
cor: FFFFFF
---
<iframe src="https://www.youtube.com/embed/E-tpeOM5TaY" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<br>

<h3>
<a target="_blank" href="https://forms.gle/tgVKM9pnGYjXMtKn7">
Inscreva-se aqui para participar!
</a>
</h3>
<br>
