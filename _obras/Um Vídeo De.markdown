---
layout: obra
thumbnail: /assets/thumbnail/obra_20.png
artista: Xikão Xikão
title: Um Vídeo De (Série)
material: Vídeo
dimensao: 5'38'', 5'38'', 5'55''
ano: 2021
bio: https://xikaoxikao.com/
cor: 969696
---
<iframe src="https://www.youtube.com/embed/Ye3otGZYBHU" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<br>
<iframe src="https://www.youtube.com/embed/xiko-mWlP88" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<br>
<iframe src="https://www.youtube.com/embed/dcZ-wkd-seE" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<br>