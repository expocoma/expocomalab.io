---
layout: obra
thumbnail: /assets/thumbnail/obra_13.png
artista: Waleff Dias
title: Defumação ou ação para a reconstrução (Série)
material: Fotografia digital 
dimensao: 
ano: 2021
bio: https://waleffdiasc.wixsite.com/portfolio
cor: 969696
---

<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
 
  	<!-- itens -->
    
   <div class="carousel-item active">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/waleff/w-1.jpeg/?auto=no&bg=777&fg=555&text=Foto01" alt="Foto01">
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/waleff/w-2.jpeg/?auto=no&bg=666&fg=444&text=Foto02" alt="Foto02">
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/waleff/w-3.jpeg/?auto=no&bg=555&fg=333&text=Foto01" alt="Foto03">
    </div>
      <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/waleff/w-4.jpeg/?auto=no&bg=555&fg=333&text=Foto01" alt="Foto04">
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/waleff/w-5.jpeg/?auto=no&bg=555&fg=333&text=Foto01" alt="Foto05">
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/waleff/w-6.jpeg/?auto=no&bg=555&fg=333&text=Foto01" alt="Foto06">
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/waleff/w-7.jpeg/?auto=no&bg=555&fg=333&text=Foto01" alt="Foto07">
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/waleff/w-8.jpeg/?auto=no&bg=555&fg=333&text=Foto01" alt="Foto08">
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/waleff/w-9.jpeg/?auto=no&bg=555&fg=333&text=Foto01" alt="Foto09">
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/waleff/w-10.jpeg/?auto=no&bg=555&fg=333&text=Foto01" alt="Foto10">
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/waleff/w-11.jpeg/?auto=no&bg=555&fg=333&text=Foto01" alt="Foto11">
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid mx-auto w-100" src="/obras-externas/waleff/w-12.jpeg/?auto=no&bg=555&fg=333&text=Foto01" alt="Foto12">
    </div>
    

    <!-- fim itens -->
  </div>
  
  <!-- controle --> 
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Anterior</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Próximo</span>
  </a>
</div>