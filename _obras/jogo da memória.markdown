---
layout: obra
thumbnail: /assets/thumbnail/obra_4.png
artista: Diana Medina
title: Jogo da memória
material: Web-art
dimensao:
ano: 2021 
bio: https://www.instagram.com/ladydipics/
cor: 00BDFF
---

<iframe style="width: 200%; height: 900px; transform: scale(0.5); transform-origin: 0 0; margin-bottom: -30em;border: none;" src="http://jogo-da-memoria-c9ad8.web.app"></iframe>

<a href="http://jogo-da-memoria-c9ad8.web.app" target="_blank">TELA INTEIRA</a>