---
layout: obra
thumbnail: /assets/thumbnail/obra_5.png
artista: Havane Melo
title: O espetáculo do céu inteligível
material: Vídeo
dimensao: 3'47''
ano: 2021
bio: https://www.havanemelo.com/
cor: 00BDFF
---
<iframe src="https://www.youtube.com/embed/7c7GiSrkjzs" width="832" height="468" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<br>
