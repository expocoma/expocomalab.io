---
layout: obra
thumbnail: /assets/thumbnail/obra_12.png
artista: Robson Castro
title: Origens (Série)
material: Fotoperformance em fotografia digital 
dimensao: (Foto de Studio Sartory) 
ano: 2021
bio:  http://www.robsoncastro.art.br/
cor: 00EBB7
---
<style>
#myImg {
  cursor: pointer;
  transition: 0.3s;
}

#myImg:hover {
  opacity: 0.7;
}

.modal {
  display: none;
  /* Hidden by default */
  position: fixed;
  /* Stay in place */
  z-index: 1;
  /* Sit on top */
  padding-top: 100px;
  /* Location of the box */
  left: 0;
  top: 0;
  width: 100%;
  /* Full width */
  height: 100%;
  /* Full height */
  overflow: auto;
  /* Enable scroll if needed */
  background-color: rgb(0, 0, 0);
  /* Fallback color */
  background-color: rgba(0, 0, 0, 0.9);
  /* Black w/ opacity */
}

.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

.modal-content,
#caption {
  animation-name: zoom;
  animation-duration: 0.6s;
}

@keyframes zoom {
  from {
    transform: scale(0)
  }
  to {
    transform: scale(1)
  }
}

.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

@media only screen and (max-width: 700px) {
  .modal-content {
    width: 100%;
  }
}
</style>


<img class="myImages" id="myImg" src="/obras-externas/robson/r-1.jpg" height="160"><br>
<img class="myImages" id="myImg" src="/obras-externas/robson/r-2.jpg" height="160"><br>
<img class="myImages" id="myImg" src="/obras-externas/robson/r-3.jpg" height="160"><br>
<img class="myImages" id="myImg" src="/obras-externas/robson/r-4.jpg" height="160"><br>
<img class="myImages" id="myImg" src="/obras-externas/robson/r-5.jpg" height="160"><br>
<img class="myImages" id="myImg" src="/obras-externas/robson/r-6.jpg" height="160"><br>
<img class="myImages" id="myImg" src="/obras-externas/robson/r-7.jpg" height="160"><br>
<img class="myImages" id="myImg" src="/obras-externas/robson/r-8.jpg" height="160"><br>
<img class="myImages" id="myImg" src="/obras-externas/robson/r-9.jpg" height="160"><br>
<img class="myImages" id="myImg" src="/obras-externas/robson/r-10.jpg" height="160">

<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
</div>



<script>
// create references to the modal...
var modal = document.getElementById('myModal');
// to all images -- note I'm using a class!
var images = document.getElementsByClassName('myImages');
// the image in the modal
var modalImg = document.getElementById("img01");
// and the caption in the modal

// Go through all of the images with our custom class
for (var i = 0; i < images.length; i++) {
  var img = images[i];
  // and attach our click listener for this image.
  img.onclick = function(evt) {
    console.log(evt);
    modal.style.display = "block";
    modalImg.src = this.src;
  }
}

var span = document.getElementsByClassName("close")[0];

span.onclick = function() {
  modal.style.display = "none";
}
</script>
