---
layout: obra
thumbnail: /assets/thumbnail/obra_8.png
artista: Taís Koshino
title: jardim | garden | 枯山水
material: Simulação Digital
dimensao: Programação por KOSHA (Rodrigo Koshino) e Mut.
ano: 2021
bio: https://taiskoshino.com/
cor: 00EBB7
---

<iframe style="width: 200%; height: 900px; transform: scale(0.5); transform-origin: 0 0; margin-bottom: -30em;border: none;" src="https://itch.io/embed-upload/3656775?color=ffffff"></iframe>

<a href="https://itch.io/embed-upload/3656775?color=ffffff" target="_blank">TELA INTEIRA</a>

<br>

<p><a href="https://docs.google.com/forms/d/e/1FAIpQLSeBiFByE7gbYZncuoECGkib2RpwrIys6sFQJ7WaugbrkfZkUw/viewform?usp=sf_link"> Envie aqui </a> a captura de tela de seu jardim e faça parte de <a href="/galeria-jardim">jardins de fuga</a>, galeria de imagens criadas dentro de jardim | 枯山水 | garden.</p>

<p><a href="https://docs.google.com/forms/d/e/1FAIpQLSeBiFByE7gbYZncuoECGkib2RpwrIys6sFQJ7WaugbrkfZkUw/viewform?usp=sf_link">Submit here </a> your garden's screenshot and be a part of <a href="/galeria-jardim">jardins de fuga (scape gardens)</a>, a gallery of images created inside jardim | 枯山水 | garden</p>
