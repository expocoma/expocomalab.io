---
layout: obra
thumbnail: /assets/thumbnail/obra_14.png
artista: Ana Paula Barbosa
title: A quem endereçar (Série)
material: Fotografia digital e cartões postais impressos em papel Filiperson branco 30% algodão 280g
dimensao: 10cm x 15cm 
ano: 2021
bio:   https://www.anapaulabarbosa.art/
cor: 969696
---
<p style="text-align: justify; width: 60%;">
Envio postais de poeiras viajantes que coabitaram minha morada neste espaço/tempo confinado.<br>
Escolheram alojar-se aqui, no decurso das suas jornadas de não-existência.<br>
Buscaram desvencilhar-se de suas origens, envolveram-se com outras matérias e tentaram, incessantemente, transformar-se substancialmente, contudo, sem êxito.<br>
No entanto, foram hábeis em orquestrar novas vistas. Errantes, espalharam-se após minha tentativa de eliminação destes persistentes indícios da finitude.<br>
Algumas ainda permanecem por aqui, deslocadas, silenciosas, em prontidão para o próximo percurso.
</p>
<img class="img-fluid " src="/obras-externas/ana/a-1.png" >
<img class="img-fluid " src="/obras-externas/ana/a-2.png" >
<br><br>
<img class="img-fluid " src="/obras-externas/ana/a-3.png" >
<img class="img-fluid " src="/obras-externas/ana/a-4.png" >
<br><br>
<img class="img-fluid " src="/obras-externas/ana/a-5.png" >
<img class="img-fluid " src="/obras-externas/ana/a-6.png" >


`Envie ` [aqui](https://docs.google.com/forms/d/e/1FAIpQLSdikQkXYiQ9snWUhR3le4xRPaeDtSQnKenX9Qeo0__qf5B4aQ/viewform)` seus dados para receber o postal.`