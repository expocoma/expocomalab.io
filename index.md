---
title: Home
layout: default
pagination: 
  enabled: true
---
<div class="container-fluid">
  <div class="row">
  {% for post in paginator.posts %}
    <div class="card  col-lg-4 col-xl-3">
                 <a  href="{{ BASE_PATH }}{{ post.url | remove: '/index.html' }}" class="shuf">
          <img class="box" style="box-shadow: 0 0 10px 0 #{{ post.cor }} inset, 0 0 15px 6px #{{ post.cor }};" src="{{ post.thumbnail }}" /> 
 
      </a>
    </div>

  {% endfor %}
  </div>
  <div class="row">
    <div class="pagina col-sm-11">
      {% if paginator.page_trail %}
        {% for trail in paginator.page_trail %}
          <li {% if page.url == trail.path %}{% endif %}>
            <a href="{{ trail.path | prepend: site.baseurl }}" title="{{trail.title}}">{{ trail.num }}</a>.
          </li>
        {% endfor %}
      {% endif %}
    </div>
  </div>
</div>

<!--           <img class="box" style="box-shadow: 0 0 10px 0 {{ post.cor }} inset, 0 0 15px 6px {{ post.cor }};" src="{{ post.thumbnail }}" /> 
 -->