---
title: Ficha Técnica
layout: default
---

<div class="container-fluid" style="width: 70%;">
  <div class="row">
<div class="ficha-tecnica col-sm-10">
  <h1>Ficha <br>Técnica</h1>
  <p><b>Curadoria:<br></b> Ana Maria Bernal<br> Artur Cabral<br> Lynn Carone <br> Mario Caillaux<br> Tais Aragão<br> Thaís Oliveira </p>
  
  <p><b>Design:</b><br>Lucas Lira</p>
  <p><b>Desenvolvimento de Plataforma:<br></b> Artur Cabral</p>

  
<b>Artistas:</b><br>
    <span>
      Alina Duchrow<br>
	  Ana Paula Barbosa<br>
	  Coletiva Arte e Maternagem (AeM)<br>
	  Danilo Piermatei<br>
	  Diana Medina<br>
	  Doug Firmino<br>
	  Fernando Pericin<br>
	  Havane Melo<br>
	  leaColumbi<br>
	  Letícia Miranda<br>
	  Lynn Carone<br>
	  Marta Mencarini<br>
	  Rafael da Escóssia<br>
	  Renato Medeiros<br>
	  Robson Castro<br>
	  Rosa Schramm<br>
	  Taís Koshino<br>
	  Thaís Oliveira<br>
	  Waleff Dias<br>
	  Xikão Xikão<br>
    </span>
<br><br>
  <p> <i>O presente trabalho foi realizado com apoio da Coordenação de Aperfeiçoamento de Pessoal de Nível Superior - Brasil (CAPES) - Código de Financiamento 001</i></p>
</div>
</div>
</div>


