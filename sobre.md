---
title: Sobre
layout: default
---

<div class="container-fluid" style="width: 70%;">

<div class="row">
    <div class="sobre col-sm-10">
      <img src="/assets/logo-linhas.png" width="350px" style="padding-bottom: 50px">
      <h5>
  		Em uma definição formal, a linha é a ligação de dois ou mais pontos. Mas este traçado, aparentemente simples, esconde diversos significados, desde um caminho a ser percorrido até a fronteira de um espaço. Foi na busca por outras interpretações que o conceito desta exposição, <b>Linhas de Fuga</b>, gestou-se. Ao mesmo tempo em que as linhas reúnem pontos distintos - os artistas, os trabalhos e o público - elas também traçam perspectivas, escapes e realidades. Em determinados momentos, elas podem ser retas e cortantes; já em outros instantes, são sinuosas e escorregadias.
      </h5>
      <h5>Os pontos, neste caso, as obras, lugares de convergência e pensamento, estendem-se uns aos outros criando sentidos diversos, atraindo e formando novas conexões, delineando novas rotas por onde fugir. Em muitos casos, as obras expostas e a realidade a que nos desvelam, tocam nas nossas feridas e nas nossas lutas contemporâneas. Em outros momentos, nos acolhem em leves devaneios e acalantos poéticos. Ao visitar a exposição, o espectador não irá encontrar um percurso predeterminado - caberá a ele conectar esses pontos, traçando assim sua própria linha de fuga. O simples ato de percorrer a exposição, mesmo que de forma virtual, deixa um rastro, um desenho rizomático. Não existe uma direção predeterminada - as associações são múltiplas e fluidas.</h5>
      <h5>Todavia, pensando sobre esse conjunto plural de obras, nós da curadoria e do educativo reunimos quatro grandes conceitos operativos como trilhas possíveis.  Ao longo do site/exposição elas estão indicadas pelas seguintes cores: <mark style="background-color:#00BDFF;padding: 0;">Impermanência / Deslocamento</mark>; <mark style="background-color:#969696;padding: 0;">Cotidiano / Corpo</mark>; <mark style="background-color: #00EBB7;padding: 0;">Movimento / Vestígio</mark>; <mark style="background-color:#FFFFFF;padding: 0;">Palavra / Cartografia</mark>. Assim, convidamos vocês à exploração e à formação de suas próprias Linhas de Fuga.</h5>
      <br>
      <p><i>A exposição Linhas de fuga é parte da IX edição do evento Coletivo em Artes Visuais (CoMA), organizado bienalmente pelo corpo discente da Pós-Graduação em Artes Visuais da Universidade de Brasília (PPGAV-UnB) e apoiado pela coordenação e corpo docente do Departamento de Artes Visuais (VIS).</i></p>
    </div>
  </div>
</div>
