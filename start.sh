#!/bin/sh

JEKYLL_ENV=production
LC_ALL=C.UTF-8
gem install bundler
bundle install
bundle exec jekyll build -d test
bundle exec jekyll serve --watch --drafts --force_polling --verbose --host 0.0.0.0
